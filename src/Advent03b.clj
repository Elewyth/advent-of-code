(ns Advent03b
  (:require [clojure.math.combinatorics :as comb]))

(defn get-rotated-vector [v]
  (if (-> (get v 0) (= 0))
    (if (-> (get v 1) (= 1))
      [-1 0]
      [1 0])
    (if (-> (get v 0) (= 1))
      [0 1]
      [0 -1])))

(defn get-step-vector [v size i k]
  (if (< i size)
        [v size (inc i) k]
        (if (= k 0)
          [(get-rotated-vector v) size 1 1]
          [(get-rotated-vector v) (inc size) 1 0])))

(defn calculate-sum-of-neighbors [map, c]
  (let [x (get (vec c) 0) y (get (vec c) 1)]
    (+
      (get map [(inc x) (inc y)] 0)
      (get map [(inc x) y] 0)
      (get map [(inc x) (dec y)] 0)
      (get map [x (inc y)] 0)
      (get map [x (dec y)] 0)
      (get map [(dec x) (inc y)] 0)
      (get map [(dec x) y] 0)
      (get map [(dec x) (dec y)] 0))))

(defn calculate-coordinates [number]
  (loop [n 1
         c [0 0]
         m {[0 0] 1}
         s 0
         v [1 0]
         size 1
         i 0
         k 0]
    (if (> s number)
      s
      (let [[v2 size2 i2 k2] (get-step-vector v size i k)
            c2 (-> (map + c v2) doall)
            s (calculate-sum-of-neighbors m c2)]
        (recur
          (inc n)
          c2
          (assoc m c2 s)
          s
          v2
          size2
          i2
          k2)))))

(defn result [input]
  (-> (calculate-coordinates input)
      (println)))

(result 368078)