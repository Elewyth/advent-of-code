(ns Advent03a)

(defn get-rotated-vector [v]
  (if (-> (get v 0) (= 0))
    (if (-> (get v 1) (= 1))
      [-1 0]
      [1 0])
    (if (-> (get v 0) (= 1))
      [0 1]
      [0 -1])))

(defn get-step-vector [v size i k]
  (if (< i size)
        [v size (inc i) k]
        (if (= k 0)
          [(get-rotated-vector v) size 1 1]
          [(get-rotated-vector v) (inc size) 1 0])))

(defn calculate-coordinates [number]
  (loop [n 1
         c [0 0]
         v [1 0]
         size 1
         i 0
         k 0]
    (if (= n number)
      c
      (let [[v2 size2 i2 k2] (get-step-vector v size i k)]
        (recur (inc n) (-> (map + c v2) doall) v2 size2 i2 k2)))))

(defn calculate-manhattan-distance [c]
  (transduce (map #(Math/abs %)) + c))

(defn result [input]
  (-> (calculate-coordinates input)
      (calculate-manhattan-distance)
      (println)))

(result 368078)